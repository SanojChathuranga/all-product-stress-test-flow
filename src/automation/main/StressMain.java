package automation.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StressMain {

	String addfirst = "F";
	String addsecond = "A";
	String addthird = "H";
	boolean doflight = true;
	boolean dohotel = true;
	boolean doacivity = true;
	public String type = "Firefoxpro";
	public String firefoxprfpath = "C:\\Users\\Sanoj\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\t7v2fp3b.default-1417678199350";
	public StringBuffer ReportPrinter = new StringBuffer();
	public String reportPath = "Reports/AllTest.html";
	public WebDriver driver = null;
	public boolean login = false;
	public boolean pass = false;
	public String userName = "Dan1234";
	public String passWord = "123456";
	public String portalUrl = "http://dev3.rezg.net/rezbase_v3";
	// public String portalUrl = "http://v3proddev2.rezg.net/rezproduction";
	// public String portalUrl = "http://oman-res-live.secure-reservation.com/omanairholidays";

	// FLIGHT SEARCH DETAILS
	public boolean ipenabled = true;
	public String FSearchType = "";
	public String FSellingCurrency = "USD";
	public String FCountry = "USA";
	public String FTriptype = "Round Trip";
	// String FTriptype = "One Way Trip";
	public String FFrom = "Dubai|DXB|4|Dubai|-|53||United Arab Emirates";
	public String FTo = "London|LHR|1112|Heathrow|-|54||United Kingdom";
	public String FDepartureDate = "1/19/2016";
	public String FDepartureTime = "AnyTime";
	public String FReturnDate = "1/23/2016";
	public String FReturnTime = "AnyTime";
	public boolean Flexible = false;
	public String FAdult = "2";
	public String FChildren = "2";
	String FChildrenAge = "5/7";
	public String Finfant = "1";
	public String FCabinClass = "Economy";
	public String FPreferredCurrency = "USD";
	public String FPreferredAirline = "";
	public boolean FNonStop = false;
	public String FselectingFlight = "1";

	// ACTIVITY SEARCH DETAILS
	public String AsellingCurrency = "USD";
	public String Acountry = "USA";
	public String Adestination = "1112|London||54||United Kingdom|";
	public String AdateFrom = "01/16/2016";
	public String AdateTo = "01/22/2016";
	public String Aadults = "2";
	public String Achildren = "2";
	public String AageOfChildren = "4/6";
	public String AchildrenAge = "2/3/4";
	public String AprogramCategory = "";
	public String ApreferCurrency = "USD";
	public String ApromotionCode = "";
	// public String AactivityName = "St Paul s Cathedral - General admission - seniors over 60";
	public String AactivityName = "NetRateTestActivityWithTax1";
	// ArrayList<String> AactivityType = new ArrayList<String>(Arrays.asList("General admission - seniors over 60"));
	ArrayList<String> AactivityType = new ArrayList<String>(Arrays.asList("50Km Rafting"));
	public String AactivityDate = "20-Jan-2016";
	public String Aselectingactivity = "1";

	// HOTEL SEARCH DETAILS
	public String Hlocation = "1112|London||54||United Kingdom|";
	public String Hcountry = "USA";
	public String Hroomcount = "3";
	public String Hresidence = "USA";
	public String Hdeparture = "01/16/2016";
	public String Hnights = "4";
	public String HAdults = "2/1/2";
	public String HChildren = "1/2/1";
	public String HChildrenAge = "6/3/8/3";
	public String Hdiscount = "";
	boolean discount = false;
	public String HselectingHotel = "2";

	@Before
	public void born() {

	}

	@Test
	public void live() {

		try {

			driver = initalizeDriver(type, firefoxprfpath);
			reportHeading("All Product Stress Testing - ", ReportPrinter);
			
			System.out.println("========TEST 1========"); 
			addfirst = "F"; 
			addsecond = "A"; 
			addthird = "H"; 
			doflight = true; 
			dohotel = true; 
			doacivity = true; 
			runMultiple(); 
			validate(driver,
			ReportPrinter, "1"); System.out.println("******END TEST 1******");
			
			/*System.out.println("========TEST 2========");
			addfirst = "H"; 
			addsecond = "A"; 
			addthird = "F"; 
			doflight = false; 
			dohotel = true; 
			doacivity = true; 
			runMultiple(); 
			validate(driver, ReportPrinter, "2"); 
			System.out.println("******END TEST 2******");*/
			
			/*System.out.println("========TEST 3========"); 
			addfirst = "H"; 
			addsecond = "A"; 
			addthird = "F"; 
			doflight = true; 
			dohotel = true; 
			doacivity = true; 
			runMultiple(); 
			validate(driver, ReportPrinter, "3"); 
			System.out.println("******END TEST 3******");
			
			System.out.println("========TEST 4========"); 
			addfirst = "H"; 
			addsecond = "F"; 
			addthird = "A"; 
			doflight = true; 
			dohotel = true; 
			doacivity = true; 
			runMultiple(); 
			validate(driver,
			ReportPrinter, "4"); System.out.println("******END TEST 4******");
			
			System.out.println("========TEST 5========"); 
			addfirst = "A"; 
			addsecond = "F"; 
			addthird = "H"; 
			doflight = true; 
			dohotel = true; 
			doacivity = true; 
			runMultiple(); 
			validate(driver, ReportPrinter, "5"); 
			System.out.println("******END TEST 5******");
			
			System.out.println("========TEST 6========");
			addfirst = "F";
			addsecond = "H";
			addthird = "A";
			doflight = true;
			dohotel = true;
			doacivity = true;
			runMultiple();
			validate(driver, ReportPrinter, "6");
			System.out.println("******END TEST 6******");*/

			writeReport(ReportPrinter, reportPath);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@After
	public void die() {

		driver.quit();

	}

	public void runMultiple() {

		try {

			driver.get(portalUrl.concat("Reservations/"));

			if (addfirst.equals("F") && doflight) {
				try {

					WebDriverWait wait = new WebDriverWait(driver, 120);
					long startsrch = System.currentTimeMillis();
					System.out.println("FLIGHTS");
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame("bec_container_frame");
					} catch (Exception e) {

					}
					searchFlight(driver);
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("airridetracer_0")));
						wait.until(ExpectedConditions.presenceOfElementLocated(By.className("result-block")));
						ArrayList<WebElement> flights = new ArrayList<WebElement>(driver.findElements(By.className("result-block")));
						Thread.sleep(3000);
						if (flights.size() != 0) {
							wait.until(ExpectedConditions.presenceOfElementLocated(By.className("continue-shopping-btn")));
							wait.until(ExpectedConditions.elementToBeClickable(By.className("continue-shopping-btn")));
							ArrayList<WebElement> button = new ArrayList<WebElement>(driver.findElements(By.className("continue-shopping-btn")));
							button.get(Integer.parseInt(FselectingFlight)).click();
							Thread.sleep(4000);
						}
					} catch (Exception e) {

					}

					long finishsrch = System.currentTimeMillis();
					long totalTimesrch = finishsrch - startsrch;
					System.out.println("Search BE : " + totalTimesrch);
					System.out.println("=====================");

					if (addsecond.equals("H") && dohotel) {
						Thread.sleep(2000);
						System.out.println("Hotel");
						addProductHotel(driver);

						if (doacivity) {
							Thread.sleep(2000);
							addProductAcitvity(driver);
						}
					} else if (addsecond.equals("A") && doacivity) {
						Thread.sleep(2000);
						addProductAcitvity(driver);

						if (dohotel) {
							Thread.sleep(2000);
							addProductHotel(driver);
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (addfirst.equals("H") && dohotel) {
				try {
					WebDriverWait wait = new WebDriverWait(driver, 120);
					long startsrch = System.currentTimeMillis();
					System.out.println("HOTEL");
					searchHotel(driver);
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.className("result-block")));
						ArrayList<WebElement> Hotels = new ArrayList<WebElement>(driver.findElements(By.className("result-block")));
						Thread.sleep(3000);
						if (Hotels.size() != 0) {
							wait.until(ExpectedConditions.presenceOfElementLocated(By.className("continue-shopping-btn")));
							wait.until(ExpectedConditions.elementToBeClickable(By.className("continue-shopping-btn")));
							ArrayList<WebElement> button = new ArrayList<WebElement>(driver.findElements(By.className("continue-shopping-btn")));
							System.out.println("Click hotel button");
							button.get(Integer.parseInt(HselectingHotel)).click();
						}
					} catch (Exception e) {

					}

					long finishsrch = System.currentTimeMillis();
					long totalTimesrch = finishsrch - startsrch;
					System.out.println("=====================");

					if (addsecond.equals("F") && doflight) {
						Thread.sleep(2000);
						addProductFlight(driver);

						if (doacivity) {
							Thread.sleep(2000);
							addProductAcitvity(driver);
						}
					} else if (addsecond.equals("A") && doacivity) {
						Thread.sleep(2000);
						addProductAcitvity(driver);

						if (doflight) {
							Thread.sleep(2000);
							addProductFlight(driver);
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (addfirst.equals("A") && doacivity) {
				try {
					WebDriverWait wait = new WebDriverWait(driver, 120);
					long startsrch = System.currentTimeMillis();
					System.out.println("ACTIVITY");
					searchActivity(driver);
					Thread.sleep(3000);

					activityAdd(driver);

					long finishsrch = System.currentTimeMillis();
					long totalTimesrch = finishsrch - startsrch;
					System.out.println("Search BE : " + totalTimesrch);
					System.out.println("=====================");

					if (addsecond.equals("H") && dohotel) {
						Thread.sleep(2000);
						addProductHotel(driver);

						if (doflight) {
							Thread.sleep(2000);
							addProductFlight(driver);
						}
					} else if (addsecond.equals("F") && doflight) {
						Thread.sleep(2000);
						addProductFlight(driver);

						if (dohotel) {
							Thread.sleep(2000);
							addProductHotel(driver);
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {

		}

	}

	// Support Methods
	public boolean login(WebDriver driver, String userName, String passWord, String portalUrl) throws IOException {

		long start = System.currentTimeMillis();
		try {
			driver.get(portalUrl.concat("/admin/common/LoginPage.do"));
			Thread.sleep(5000);
			driver.findElement(By.id("user_id")).clear();
			driver.findElement(By.id("user_id")).sendKeys(userName);
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys(passWord);
			driver.findElement(By.id("loginbutton")).click();
			driver.findElement(By.id("mainmenurezglogo"));
			long finish = System.currentTimeMillis();
			long totalTime = finish - start;
			System.out.println("Login total time : " + totalTime);
			return true;
		} catch (Exception e) {
			// long finish = System.currentTimeMillis();
			// long totalTime = finish - start;
			return false;
		}
	}

	public void searchFlight(WebDriver driver) {

		long start = System.currentTimeMillis();

		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			try {
				driver.findElement(By.className("show_filter")).click();
			} catch (Exception e3) {
				e3.printStackTrace();
			}

			if (!ipenabled) {
				try {
					Thread.sleep(3000);
					new Select(driver.findElement(By.id("F_Country"))).selectByVisibleText(FCountry);
				} catch (Exception e) {
					System.out.println("This is a TO Booking dude... No residence country is required...");
				}
			}

			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("air_Loc_a")));
				Thread.sleep(3000);

				driver.findElement(By.id("air_Loc_a")).sendKeys(FFrom.trim().split("\\|")[0]);
				String id = "hid_air_Loc_a";
				((JavascriptExecutor) driver).executeScript("document.getElementById('" + id + "').setAttribute('value','" + FFrom.trim() + "');");

				driver.findElement(By.id("air_Loc1_a")).sendKeys(FTo.trim().split("\\|")[0]);
				((JavascriptExecutor) driver).executeScript("document.getElementById('hid_air_Loc1_a').setAttribute('value','" + FTo.trim() + "');");

			} catch (Exception e) {

			}

			if (FTriptype.equals("Round Trip")) {

				((JavascriptExecutor) driver).executeScript("$('#air_departure').val('" + FDepartureDate + "');");
				((JavascriptExecutor) driver).executeScript("$('#air_arrival').val('" + FReturnDate + "');");

			} else if (FTriptype.equals("One Way Trip")) {

				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Air_TripType1")));
				((JavascriptExecutor) driver).executeScript("$('#Air_TripType1').click();");
				try {
					driver.findElement(By.id("Air_TripType1")).click();
				} catch (Exception e) {
					e.printStackTrace();
				}

				Thread.sleep(4000);
				try {
					((JavascriptExecutor) driver).executeScript("$('#Air_TripType1').click();");
				} catch (Exception e) {
					e.printStackTrace();
				}

				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("air_departure")));
				((JavascriptExecutor) driver).executeScript("$('#air_departure').val('" + FDepartureDate.trim() + "');");
				new Select(driver.findElement(By.id("Air_DepTime_a"))).selectByVisibleText(FDepartureTime.trim());
			}

			// FLEXIBLE FLIGHTS
			/*
			 * if (Boolean.valueOf(FisFlexible)) { driver.findElement(By.id(propertyMap.get("HmPg_Flexible_ChkBox_id"))).click(); }
			 */

			// =========================PASSENGERS==========================
			// ADULTS
			try {
				new Select(driver.findElement(By.id("R1occAdults_F"))).selectByValue(FAdult);
			} catch (Exception e) {

			}

			// CHILDREN
			if (!FChildren.trim().equals("0")) {

				new Select(driver.findElement(By.id("R1occChildren_F"))).selectByValue(FChildren.trim());

				String value = "";
				value = "F_R1childage_REPLACE";
				try {
					for (int i = 1; i <= Integer.parseInt(FChildren); i++) {
						value = value.replace("REPLACE", String.valueOf(i));
						new Select(driver.findElement(By.id(value))).selectByValue(FChildrenAge.split("/")[i - 1].trim());// Child Loop
					}
				} catch (Exception e) {

				}
			}

			// INFANT
			if (!Finfant.trim().equals("0")) {

				new Select(driver.findElement(By.id("R1occInfant_F"))).selectByValue(Finfant.trim());
			}

			// CABIN CLASS
			new Select(driver.findElement(By.id("Aira_FlightClass"))).selectByValue(FCabinClass.trim());

			// PREFERRED CURRENCY
			if (!FPreferredCurrency.trim().equals("Select Currency")) {
				new Select(driver.findElement(By.id("F_consumerCurrencyCode"))).selectByValue(FPreferredCurrency.trim());
			}

			// PREFER NONSTOP
			/*
			 * try { if (true) { driver.findElement(By.xpath("/html/body/form[2]/div/div/div[1]/div/div/ul/li[8]/ul[2]/li[5]/input")).click(); } } catch (Exception e1) { e1.printStackTrace(); }
			 */

		} catch (Exception e) {

		}
		long finish = System.currentTimeMillis();
		long totalTime = finish - start;
		System.out.println("Load BE : " + totalTime);

		try {
			((JavascriptExecutor) driver).executeScript("JavaScript:search('F');");
		} catch (Exception e) {
			try {
				driver.findElement(By.id("search_btns_f")).click();
			} catch (Exception e2) {

			}
		}

	}

	public void searchHotel(WebDriver driver) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		long start = System.currentTimeMillis();
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);

		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("bec_container_frame");
		} catch (Exception e) {

		}

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='hLi']/a")));
		} catch (Exception e) {

		}
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='hLi']/a")).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("norooms_H")));
		// 1112|London||54||United Kingdom|
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		((JavascriptExecutor) driver).executeScript("document.getElementById('hid_H_Loc').setAttribute('value','" + Hlocation + "');");
		new Select(driver.findElement(By.id("norooms_H"))).selectByVisibleText(Hroomcount.trim());

		String[] adultCount = HAdults.split("/");
		int adultExcelIterator = 1;
		try {
			for (int i = 0; i < adultCount.length; i++) {
				new Select(driver.findElement(By.id("R" + adultExcelIterator + "occAdults_H"))).selectByVisibleText(adultCount[i]);
				adultExcelIterator++;
			}
		} catch (Exception e) {

		}

		int childExcelIterator = 1;
		String[] childCount = HChildren.split("/");
		int ageExcelIterator = 0;
		try {
			for (int i = 0; i < childCount.length; i++) {
				new Select(driver.findElement(By.id("R" + childExcelIterator + "occChildren_H"))).selectByVisibleText(childCount[i]);
				childExcelIterator++;
			}
		} catch (Exception e) {

		}

		String[] age = HChildrenAge.split("/");
		int temp = 0;
		try {
			for (int j = 1; j > 0; j++) {
				if (childCount[temp].equals(null) || childCount[temp].equals("")) {
					break;
				}

				for (int k = 1; k > 0; k++) {
					try {
						new Select(driver.findElement(By.id("H_R" + j + "childage_" + k))).selectByVisibleText(age[ageExcelIterator]);
						ageExcelIterator++;

					} catch (Exception e) {
						break;
					}
				}
				temp++;
			}
		} catch (Exception e) {

		}

		((JavascriptExecutor) driver).executeScript("$('#ho_departure').val('" + Hdeparture + "');");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		String dt = null;
		try {
			c.setTime(sdf.parse(Hdeparture));
			c.add(Calendar.DATE, Integer.parseInt(Hnights));
			dt = sdf.format(c.getTime());
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		Thread.sleep(1000);

		((JavascriptExecutor) driver).executeScript("$('#ho_arrival').val('" + dt + "');");

		String location = Hlocation.replace("|", "/");
		String[] loc = location.split("/");
		driver.findElement(By.id("H_Loc")).sendKeys(loc[1].trim());
		try {
			new Select(driver.findElement(By.id("H_Country"))).selectByVisibleText(Hcountry);
		} catch (Exception e) {

		}

		// apply discount
		if (discount) {
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='hotelDisplay']/div/ul/li[5]/div/a")).click();
			driver.findElement(By.id("discountCoupon_No_H")).sendKeys(Hdiscount);
		}

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search_btns_h")));
		} catch (Exception e) {

		}
		try {
			Thread.sleep(2000);
			driver.findElement(By.id("search_btns_h")).click();
		} catch (Exception e) {

		}

		long finish = System.currentTimeMillis();
		long totalTime = finish - start;
	}

	public void searchActivity(WebDriver driver) throws InterruptedException, IOException {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("bec_container_frame");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hLi")));

		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);

		Thread.sleep(5500);
		driver.findElement(By.id("ac_departure_temp")).click();

		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);

		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).click();
		Thread.sleep(500);

		Thread.sleep(500);

		driver.findElement(By.xpath(".//*[@id='activityDisplay']/div/ul/li[3]/div/a")).click();
		Thread.sleep(500);

		driver.findElement(By.xpath(".//*[@id='aLi']/a/span")).click();
		Thread.sleep(1500);

		try {
			new Select(driver.findElement(By.id("AC_Country"))).selectByVisibleText(Acountry.trim());
		} catch (Exception e) {

			// e.printStackTrace();
		}

		String hiddenDest = Adestination;
		driver.findElement(By.id("activity_Loc")).clear();
		driver.findElement(By.id("activity_Loc")).sendKeys(hiddenDest);
		Thread.sleep(2000);

		String id = "hid_AC_Loc";
		((JavascriptExecutor) driver).executeScript("document.getElementById('" + id + "').setAttribute('value','" + Adestination.trim() + "');");
		// ((JavascriptExecutor) driver).executeScript("document.getElementById('hid_AC_Loc').setAttribute('value','" + Adestination.trim() + "');");

		// driver.findElement(By.partialLinkText(hiddenDest)).click();

		((JavascriptExecutor) driver).executeScript("$('#ac_departure').val('" + AdateFrom + "');");
		((JavascriptExecutor) driver).executeScript("$('#ac_arrival').val('" + AdateTo + "');");

		new Select(driver.findElement(By.id("R1occAdults_A"))).selectByVisibleText(Aadults);
		new Select(driver.findElement(By.id("R1occChildren_A"))).selectByVisibleText(Achildren);

		// CHILDREN
		if (!Achildren.trim().equals("0")) {

			new Select(driver.findElement(By.id("R1occChildren_A"))).selectByVisibleText(Achildren.trim());

			String value = "";
			value = "A_R1childage_REPLACE";
			try {
				for (int i = 1; i <= Integer.parseInt(Achildren); i++) {
					value = value.replace("REPLACE", String.valueOf(i));
					new Select(driver.findElement(By.id(value))).selectByValue(AchildrenAge.split("/")[i - 1].trim());// Child Loop
				}
			} catch (Exception e) {

			}
		}

		((JavascriptExecutor) driver).executeScript("search('A');");

		long firstSearch = (System.currentTimeMillis() / 1000);
	}

	public void addProductFlight(WebDriver driver) {

		try {
			driver.switchTo().defaultContent();

			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("popup-container-inner")));
			Thread.sleep(5000);
			if (driver.findElement(By.className("popup-container-inner")).isDisplayed()) {
				System.out.println("@#$%^&*()");
				driver.findElement(By.partialLinkText("Add Flights")).click();
				driver.switchTo().frame("bec_container_frame_WJ_13");
				searchFlight(driver);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("airridetracer_0")));
				wait.until(ExpectedConditions.elementToBeClickable(By.className("result-block")));
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("result-block")));
				ArrayList<WebElement> flights = new ArrayList<WebElement>(driver.findElements(By.className("result-block")));
				if (flights.size() != 0) {
					wait.until(ExpectedConditions.elementToBeClickable(By.className("continue-shopping-btn")));
					ArrayList<WebElement> button = new ArrayList<WebElement>(driver.findElements(By.className("continue-shopping-btn")));
					button.get(Integer.parseInt(FselectingFlight)).click();
					System.out.println("Flight added");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addProductAcitvity(WebDriver driver) {
		try {
			driver.switchTo().defaultContent();
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("popup-container-inner")));
			Thread.sleep(5000);
			if (driver.findElements(By.className("popup-body")).size() > 0) {
				try {
					try {
						driver.findElement(By.partialLinkText("Add Activities")).click();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					wait.until(ExpectedConditions.elementToBeClickable(By.className("result-block")));
					wait.until(ExpectedConditions.presenceOfElementLocated(By.className("result-block")));
					ArrayList<WebElement> activity = new ArrayList<WebElement>(driver.findElements(By.className("result-block")));
					if (activity.size() != 0) {
						// SELECT AN ACTIVITY
						try {
							final short ACTIVITY_PER_PAGE = 10;
							int wholepages = 1;
							int activityCount = 10;
							String idOnly = "";
							outerloop: for (int actvityPage = 1; actvityPage <= wholepages; actvityPage++) {
								for (int i = 0; i < ACTIVITY_PER_PAGE; i++) {

									if (((actvityPage - 1) * ACTIVITY_PER_PAGE) + (i) >= activityCount) {

										break outerloop;

									} else {

										String excelActivityName = AactivityName.replaceAll(" ", "");
										String webActivityName = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[1]/h1")).getText().replaceAll(" ", "");

										if (excelActivityName.equalsIgnoreCase(webActivityName)) {

											WebElement mainElement = driver.findElement(By.id("activity_results_container_" + i + ""));
											WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
											idOnly = idElement.getAttribute("code");

											new Select(driver.findElement(By.xpath(".//*[@id='" + idOnly + "_date_select']"))).selectByVisibleText(AactivityDate);

											try {
												driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[2]/ul/li[3]/a")).click();
												Thread.sleep(3500);
											} catch (Exception e) {
												e.printStackTrace();
											}

											List<WebElement> activityTypesElement = driver.findElements(By.className("activity-list-body"));
											int divCount = activityTypesElement.size();

											for (int j = 0; j < AactivityType.size(); j++) {

												String excelActivityTypeName = AactivityType.get(j).replaceAll(" ", "");

												for (int k = 1; k <= activityTypesElement.size(); k++) {

													String webActivityTypeName = "";
													try {
														webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/ul/li[1]")).getText().replaceAll(" ", "");
													} catch (Exception e) {
														try {
															webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[1]")).getText().replaceAll(" ", "");
														} catch (Exception e2) {

														}
														e.printStackTrace();
													}
													// ]/div[1]
													if (excelActivityTypeName.equalsIgnoreCase(webActivityTypeName)) {

														String idNo2;
														try {
															WebElement selectElement = null;
															try {
																selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/ul/li[4]"));
															} catch (Exception e) {
																try {
																	selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[4]"));
																} catch (Exception e2) {

																}
																e.printStackTrace();
															}
															idNo2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];

															int totalPaxCount = Integer.parseInt(Aadults) + Integer.parseInt(Achildren);
															new Select(driver.findElement(By.xpath(".//*[@id='activity_qty_select_WJ_1_" + idOnly + "_" + idNo2 + "']"))).selectByVisibleText(Integer.toString(totalPaxCount));

															Thread.sleep(1000);
															driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_" + idOnly + "_" + idNo2 + "']")).click();
															Thread.sleep(1000);
														} catch (Exception e) {
															e.printStackTrace();
														}
													}
												}
											}

											if (true) {

												// Add and Continue
												driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (divCount + 2) + "]/div/a[2]")).click();
												System.out.println("Activity added");
												Thread.sleep(5500);
											}

											break outerloop;

										}
									}
								}

								if (2 <= actvityPage) {
									driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span[" + (actvityPage + 2) + "]/a")).click();
								} else {
									driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span[" + (actvityPage + 1) + "]/a")).click();
								}

							}

						} catch (Exception e) {

						}

					}

				} catch (Exception e) {

				}
			} else {
				System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addProductHotel(WebDriver driver) {
		try {
			driver.switchTo().defaultContent();
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("popup-container-inner")));
			Thread.sleep(5000);
			if (driver.findElement(By.className("popup-container-inner")).isDisplayed()) {

				driver.findElement(By.partialLinkText("Add Hotels")).click();

				wait.until(ExpectedConditions.elementToBeClickable(By.className("result-block")));
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("result-block")));

				ArrayList<WebElement> hotels = new ArrayList<WebElement>(driver.findElements(By.className("result-block")));
				if (hotels.size() != 0) {
					wait.until(ExpectedConditions.elementToBeClickable(By.className("continue-shopping-btn")));
					ArrayList<WebElement> button = new ArrayList<WebElement>(driver.findElements(By.className("continue-shopping-btn")));
					button.get(Integer.parseInt(HselectingHotel)).click();
					System.out.println("Hotel Added");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public WebDriver initalizeDriver(String type, String profilePath) throws IOException {
		String Type = type.trim();

		if (Type.equalsIgnoreCase("FirefoxBin")) {
			FirefoxProfile prof = new FirefoxProfile();
			FirefoxBinary bin = new FirefoxBinary();
			driver = new FirefoxDriver(bin, prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			return driver;
		}
		if (Type.equalsIgnoreCase("Firefoxpro")) {
			File profile = new File(profilePath);
			FirefoxProfile prof = new FirefoxProfile(profile);
			driver = new FirefoxDriver(prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			return driver;
		} else {
			FirefoxProfile prof = new FirefoxProfile();
			driver = new FirefoxDriver(prof);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			return driver;
		}

	}

	public void activityAdd(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("result-block")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("result-block")));
		ArrayList<WebElement> activity = new ArrayList<WebElement>(driver.findElements(By.className("result-block")));
		if (activity.size() != 0) {
			// SELECT AN ACTIVITY
			try {
				final short ACTIVITY_PER_PAGE = 10;
				int wholepages = 1;
				int activityCount = 10;
				String idOnly = "";
				outerloop: for (int actvityPage = 1; actvityPage <= wholepages; actvityPage++) {
					for (int i = 0; i < ACTIVITY_PER_PAGE; i++) {

						if (((actvityPage - 1) * ACTIVITY_PER_PAGE) + (i) >= activityCount) {

							break outerloop;

						} else {

							String excelActivityName = AactivityName.replaceAll(" ", "");
							String webActivityName = driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[1]/div[2]/div[1]/h1")).getText().replaceAll(" ", "");

							if (excelActivityName.equalsIgnoreCase(webActivityName)) {

								WebElement mainElement = driver.findElement(By.id("activity_results_container_" + i + ""));
								WebElement idElement = mainElement.findElement(By.className("select-activity-date"));
								idOnly = idElement.getAttribute("code");

								new Select(driver.findElement(By.xpath(".//*[@id='" + idOnly + "_date_select']"))).selectByVisibleText(AactivityDate);

								try {
									driver.findElement(By.xpath(".//*[@id='activity_results_container_" + i + "']/div/div[1]/div[2]/ul/li[3]/a")).click();
									Thread.sleep(3500);
								} catch (Exception e) {
									// e.printStackTrace();
								}

								List<WebElement> activityTypesElement = driver.findElements(By.className("activity-list-body"));
								int divCount = activityTypesElement.size();

								for (int j = 0; j < AactivityType.size(); j++) {

									String excelActivityTypeName = AactivityType.get(j).replaceAll(" ", "");

									for (int k = 1; k <= activityTypesElement.size(); k++) {

										String webActivityTypeName = "";
										boolean crazy = false;
										try {
											webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/ul/li[1]")).getText().replaceAll(" ", "");
										} catch (Exception e) {
											try {
												webActivityTypeName = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[1]")).getText().replaceAll(" ", "");
												crazy = true;
											} catch (Exception e2) {
												
											}
											// e.printStackTrace();
										}
										// ]/div[1]
										if (excelActivityTypeName.equalsIgnoreCase(webActivityTypeName)) {

											String idNo2;
											try {
												WebElement selectElement = null;
												try {
													selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/ul/li[4]"));
												} catch (Exception e) {

												}

												try {
													if (crazy) {
														selectElement = driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (k + 1) + "]/div[4]"));
													}
												} catch (Exception e) {
													e.printStackTrace();
												}
												idNo2 = selectElement.findElement(By.tagName("select")).getAttribute("id").split("_")[6];

												int totalPaxCount = Integer.parseInt(Aadults) + Integer.parseInt(Achildren);
												new Select(driver.findElement(By.xpath(".//*[@id='activity_qty_select_WJ_1_" + idOnly + "_" + idNo2 + "']"))).selectByVisibleText(Integer.toString(totalPaxCount));

												Thread.sleep(1000);
												driver.findElement(By.xpath(".//*[@id='activity_qty_WJ_1_" + idOnly + "_" + idNo2 + "']")).click();
												Thread.sleep(1000);
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}
								}

								if (true) {

									// Add and Continue
									driver.findElement(By.xpath(".//*[@id='activity_content_WJ_1_" + idOnly + "']/div[" + (divCount + 2) + "]/div/a[2]")).click();
									System.out.println("Activity added");
									Thread.sleep(5500);
								}

								break outerloop;

							}
						}
					}

					if (2 <= actvityPage) {
						driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span[" + (actvityPage + 2) + "]/a")).click();
					} else {
						driver.findElement(By.xpath(".//*[@id='pagination_WJ_3']/span[" + (actvityPage + 1) + "]/a")).click();
					}

				}

			} catch (Exception e) {
				
			}

		}

	}

	public void validate(WebDriver driver, StringBuffer ReportPrinter, String testNo) {
		try {
			driver.switchTo().defaultContent();
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("popup-container-inner")));
			Thread.sleep(5000);
			if (driver.findElements(By.className("popup-body")).size() > 0) {
				try {
					wait.until(ExpectedConditions.elementToBeClickable(By.className("close-popup")));
					driver.findElement(By.className("close-popup")).click();
				} catch (Exception e1) {
					//e1.printStackTrace();
				}
			}
		} catch (Exception e) {

		}

		driver.switchTo().defaultContent();
		ArrayList<WebElement> cartItems = null;
		try {
			cartItems = new ArrayList<WebElement>(driver.findElements(By.className("my-basket-item-header")));
		} catch (Exception e) {
			e.printStackTrace();
		}

		int products = 0;
		if (doflight) {
			products = products + 1;
		}
		if (dohotel) {
			products = products + 1;
		}
		if (doacivity) {
			products = products + 1;
		}

		ReportPrinter.append("<span><center><p class='Hedding0'>Test Details</p></center></span>");
		ReportPrinter.append("<table style=width:100%>" + "<tr><th>Product</th>" + "<th>Test Description " + testNo + "</th></tr>");

		if (doflight) {
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Flight</td>" + "<td class='Passed'>Pass</td></tr>");
		}
		if (dohotel) {
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Hotel</td>" + "<td class='Passed'>Pass</td></tr>");
		}
		if (doacivity) {
			ReportPrinter.append("<tr>");
			ReportPrinter.append("<td>Activity</td>" + "<td class='Passed'>Pass</td></tr>");
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");

		ReportPrinter.append("<span><center><p class='Hedding0'>Products in cart</p></center></span>");
		ReportPrinter.append("<table style=width:100%>" + "<tr><th>Product</th></tr>");

		for (int i = 0; i < cartItems.size(); i++) {
			ReportPrinter.append("<tr><td>" + cartItems.get(i).getText() + "</td></tr>");
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");

	}

	public void reportHeading(String Heading, StringBuffer ReportPrinter) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>" + Heading + " (" + sdf.format(Calendar.getInstance().getTime()) + ")</p></div>");
		ReportPrinter.append("<body>");
	}

	public void writeReport(StringBuffer ReportPrinter, String filepath) {
		ReportPrinter.append("</body></html>");
		BufferedWriter bwr;
		try {
			bwr = new BufferedWriter(new FileWriter(new File(filepath)));
			bwr.write(ReportPrinter.toString());
			bwr.flush();
			bwr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Runtime.getRuntime().exec(new String[] { "cmd.exe", "/C", "E:\\workspace\\All_Product_StressTest_Flow\\Reports\\AllTest.html" });
		} catch (Exception e) {

		}
	}

}
